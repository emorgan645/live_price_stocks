import datetime
import logging

from flask import Flask, abort, jsonify, make_response
from flask_restplus import Resource, Api, fields
import pandas as pd
import pandas_datareader as pdr
import numpy as np
from io import StringIO
from flask.helpers import send_file
import yfinance as yf

## added yahoo fin for more up to date info

from flask_cors import CORS

app = Flask(__name__)
CORS(app,resources={r"/v1/*": {"origins": "*"}})

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)
   
stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    })

## New show price method

@api.route('/v1/price/<ticker>')
@api.param('ticker', 'The stock ticker to get a price for')
class Price(Resource):

    def get(self, ticker):
        ticker = ticker.split()
        LOG.info(ticker)
        startDate = datetime.datetime.now() - datetime.timedelta(30)

        returnList = []

        for tick in ticker:
            try:
                df = pdr.get_data_yahoo(tick, startDate)
                df['Date'] = df.index
                returnList.append({'Ticker': tick,
                            'Date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                            'Close': float(np.round(df['Close'][-1],2)),
                            'Open': float(np.round(df['Open'][-1],2))})
            
            except Exception as ex:
                    LOG.error('Error getting data: ' + str(ex))
                    abort(400, 'Price data error')


        return make_response(jsonify(returnList), 200)


@api.route('/v1/topStocks/')
class TopStocks(Resource):

    def get(self):
        ticker = ["AAPL", "GOOGL", "MSFT", "AMZN", "FB", "BAC", "TSLA", "C", "XOM" ]
        
        LOG.info(ticker)
        startDate = datetime.datetime.now() - datetime.timedelta(30)

        returnList = []

        for tick in ticker:
            try:
                df = pdr.get_data_yahoo(tick, startDate)
                df['Date'] = df.index
                returnList.append({'Ticker': tick,
                            'Date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                            'Close': float(np.round(df['Close'][-1],2)),
                            'Open': float(np.round(df['Open'][-1],2))})
            
            except Exception as ex:
                    LOG.error('Error getting data: ' + str(ex))
                    abort(400, 'Price data error')


        return make_response(jsonify(returnList), 200)


@api.route('/v1/advice/<ticker>')
class BuySellHold(Resource):
    def get(self, ticker):
        answerList = ["BUY", "SELL", "HOLD"]
        ticker = ticker.split()
        LOG.info(ticker)
        start_date = datetime.datetime.now() - datetime.timedelta(30)

        return_list = []
        for tick in ticker:

            df_Boll = pdr.get_data_yahoo(tick, start_date)

            df_Boll['30d mavg'] = df_Boll['Close'].rolling(window=30).mean()
            df_Boll['30d std'] = df_Boll['Close'].rolling(window=30).std()
            

            df_Boll['Upper'] = df_Boll['30d mavg'] + (2 * df_Boll['30d std'])
            df_Boll['Lower'] = df_Boll['30d mavg'] - (2 * df_Boll['30d std'])

            if (df_Boll['Close'][-1] >= df_Boll['Upper'][-1]):
                return_list.append({"Ticker:": tick, "Recommendation": answerList[1], "Close": float(np.round(df_Boll['Close'][-1],2))})
            elif (df_Boll['Close'][-1] <= df_Boll['Lower'][-1]):
                return_list.append({"Ticker:": tick, "Recommendation": answerList[0],"Close": float(np.round(df_Boll['Close'][-1],2))})
            else: return_list.append({"Ticker:": tick, "Recommendation": answerList[2],"Close": float(np.round(df_Boll['Close'][-1],2))})
        
        return  make_response(jsonify(return_list),200)

@api.route('/v1/info/<tick>')
class GetMoreInfo(Resource):
    def get(self, tick):
        returnList = ["Close to selling time", "Close to buying time", "Remain holding"]
        start_date = datetime.datetime.now() - datetime.timedelta(365)
        df_Boll = pdr.get_data_yahoo(tick, start_date)

        df_Boll['30d mavg'] = df_Boll['Close'].rolling(window=30).mean()
        df_Boll['30d std'] = df_Boll['Close'].rolling(window=30).std()

        df_Boll['Upper'] = df_Boll['30d mavg'] + (2 * df_Boll['30d std'])
        df_Boll['Lower'] = df_Boll['30d mavg'] - (2 * df_Boll['30d std'])
        
        upperRatio = ((df_Boll['Close'][-1])/(df_Boll['Upper'][-1]))
        lowerRatio = ((df_Boll['Lower'][-1])/(df_Boll['Close'][-1]))

        if(upperRatio >= 0.95):
           return (returnList[0])
        elif(lowerRatio >= 0.95):
            return (returnList[1])
        else: return (returnList[2])

@api.route('/v1/markets/')
class Market(Resource):
    def get(self):

        marketList = []

        # The below will pull back stock prices from the start date until end date specified.
        start_sp = datetime.datetime(2020, 1, 1)
        end_sp = datetime.datetime(2020, 10, 16)
        # This variable is used for YTD performance.
        end_of_last_year = datetime.datetime(2019, 12, 29)

        yf.pdr_override() 
        sp500 = pdr.get_data_yahoo('^GSPC', 
                        start_sp,
                        end_sp)
        sp500['Date'] = sp500.index
        
        marketList.append({ 'Code': "^GSPC",
                            'Name': "S&P500",
                            'Date': sp500.iloc[-1]['Date'].strftime("%d/%m/%Y"),
                            'Close': float(np.round(sp500['Close'][-1],2)),
                            'Open': float(np.round(sp500['Open'][-1],2))}),
        
        dow30 = pdr.get_data_yahoo('^DJI',
        start_sp, end_sp)
        dow30['Date'] = dow30.index

        marketList.append({'Code': "^DJI",
                        'Name': "Dow30", 
                        'Date': dow30.iloc[-1]['Date'].strftime("%d/%m/%Y"),
                        'Close': float(np.round(dow30['Close'][-1],2)),
                        'Open': float(np.round(dow30['Open'][-1],2))})

        ndaq = pdr.get_data_yahoo('^IXIC',
        start_sp, end_sp)
        ndaq['Date'] = ndaq.index

        marketList.append({'Code': "^IXIC",
                        'Name': "NASDAQ", 
                        'Date': ndaq.iloc[-1]['Date'].strftime("%d/%m/%Y"),
                        'Close': float(np.round(ndaq['Close'][-1],2)),
                        'Open': float(np.round(ndaq['Open'][-1],2))})

        rut = pdr.get_data_yahoo('^RUT',
        start_sp, end_sp)
        rut['Date'] = rut.index

        marketList.append({'Code': "^RUT",
                        'Name': "Russell 2000", 
                        'Date': rut.iloc[-1]['Date'].strftime("%d/%m/%Y"),
                        'Close': float(np.round(rut['Close'][-1],2)),
                        'Open': float(np.round(rut['Open'][-1],2))})
    
        oil = pdr.get_data_yahoo('CL=F',
        start_sp, end_sp)
        oil['Date'] = oil.index

        marketList.append({'Code': "^CL=F",
                        'Name': "Crude Oil", 
                        'Date': oil.iloc[-1]['Date'].strftime("%d/%m/%Y"),
                        'Close': float(np.round(oil['Close'][-1],2)),
                        'Open': float(np.round(oil['Open'][-1],2))})

        gold = pdr.get_data_yahoo('GC=F',
        start_sp, end_sp)
        gold['Date'] = gold.index

        marketList.append({'Code': "^GC=F",
                        'Name': "Gold", 
                        'Date': gold.iloc[-1]['Date'].strftime("%d/%m/%Y"),
                        'Close': float(np.round(gold['Close'][-1],2)),
                        'Open': float(np.round(gold['Open'][-1],2))})

        return make_response(jsonify(marketList), 200)


@api.route('/v1/change/<ticker>')
class Change(Resource):

    def get(self, ticker):
        
        startDate = datetime.datetime.now() - datetime.timedelta(30)
        returnList=[]

        df = pdr.get_data_yahoo(ticker, startDate)
        df['Date'] = df.index
        diffList=[]
        returnList=[]
        df['Change']= df['Close'] - df['Open']
        for i in range(1,6):
            returnList.append({"Date": df.iloc[-i]['Date'].strftime("%d/%m/%Y"), "Change in Stock": float(np.round(df.iloc[-i]['Change'],2))})
        
        return make_response(jsonify(returnList, 200))


    
if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)
